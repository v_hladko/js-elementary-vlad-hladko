const envelope = (first,second) => {
    let error = validateObject(first,second)
    if(error){ return error }

    if((first.width < second.width && first.height < second.height) || (first.width < second.height && first.height < second.width)){
        return 1
    }	
    else if((first.width > second.width && first.height > second.height) || (first.width > second.height && first.height > second.width)){
        return 2
    }
    else{
        return 0
    }
}

const validateObject = (...objects) => {
    let error = false
	objects.forEach(el => {
        if(typeof el !== 'object'){
            error = {
                status: 'failed',
                reason: 'Нужно передать объект'
            }
        }
        else if(typeof el.height !== 'number' || typeof el.width !== 'number'){
            error = {
                status: 'failed',
                reason: 'Параметры объекта должны быть number'
            }
        }
    })
    return error
}

let first = {
    width: 1.5,
    height: 2
}

let second = {
	width: 3,
    height: 4
}

let result = envelope(first,second)
console.log(result)