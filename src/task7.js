const getFibonacciList = context => {
    let result;
    if(context.hasOwnProperty('length')){
        result = getFibonacciByLength(context);
    }
    else if(context.hasOwnProperty('min') && context.hasOwnProperty('max')){
        result = getFibonacciByRange(context);
    }
    return result;
}

const getFibonacciByLength = context => {
    let result = new Array();
    let x = 1,
        y = 1;
    while(y.toString().length < context.length){
        y = x + y;
        x = y - x;
        result.push(y);
    }
    return result;
}

const getFibonacciByRange = context => {
    let result = new Array();
    let x = 1,
        y = 1;
    while(y <= context.max){
        y = x + y;
        x = y - x;
        if(y >= context.min && y <= context.max){
            result.push(y);
        }
    }
    return result;
}

let test = getFibonacciList({length: 10});
console.log(test);