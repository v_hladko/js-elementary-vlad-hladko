const getSquare = (length,minSquare) => {
    let originalNumber = Math.ceil(Math.sqrt(minSquare));
    let result = new Array();
    for(let i = 0;i< length; i++){
        result.push(originalNumber++);
    }
    return result.join(',');
}

let test = getSquare(10,125);
console.log(test);