const chess = (rows,column,symbol) => {
    let error,evalStr = '',oddStr = '',table = ''

    error = validateNumber(rows, column)
    if(error){	return error}
    error = validateString(symbol)
    if(error){  return error}

    for(let i = 0; i < rows; i ++ ){
        evalStr += (i%2 == 0) ? symbol : ' '
        oddStr += (i%2 != 0) ? symbol : ' '
    }
    for(let i = 0; i < column; i++){
        table += (i%2 == 0) ? `${evalStr}\n` : `${oddStr}\n`
    }
    
    return table
}

const validateNumber = (...numbers) => {
	let error = false
    numbers.forEach(el => {
        if(typeof el !== 'number'){
            error = {
                status: 'failed',
                reason: 'Необходимо ввести число'
            }
        }
      
        if(el == 0){
            error = {
                status: 'failed',
                reason: 'Число не может быть равно 0'
            }
        }
    })
    return error
}

const validateString = string => {
	let error = false
	if(typeof string !== 'string'){
  	error = {
    	status: 'failed',
      reason: 'Необходимо ввести строку'
    }
  }
  
  if(string.trim == ''){
  	error = {
    	status: 'failed',
      reason: 'Строка не может быть пустой'
    }
  }
  
  return error
}

let table = chess(2,8,'8')
console.log(table)

