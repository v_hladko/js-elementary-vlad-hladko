const getHappyTicket = context => {
    const FIRST_METHOD_INFO = 'Метод с подсчетом счастливых билетов по принципу равности суммы первых и последних трех цифр';
    const SECOND_METHOD_INFO ='Метод с подсчетом счастливых билетов по принципу равности суммы четных и нечетных цифр';
    let firstMethodResult = firstTicketMethod(context);
    let secondMethodResult = secondTicketMethod(context);

    let result = {
        'first': firstMethodResult,
        'second': secondMethodResult
    };
    if(firstMethodResult > secondMethodResult){
        result.winner = FIRST_METHOD_INFO; 
    }
    else{
        result.winner = SECOND_METHOD_INFO;
    }
    return result;
}

const firstTicketMethod = obj => {
    let cnt = 0;

    for(let i = obj.min; i <= obj.max; i++){
        let numberArray = String(i).split('').map(Number);
        let firstSum = numberArray.filter((el, index) => index < 3).reduce((sum,el) => sum + el);
        let secondSum = numberArray.filter((el, index) => index >= 3).reduce((sum,el) => sum + el); 
        if(firstSum == secondSum){
            cnt++;
        }
    }
    return cnt;
}

const secondTicketMethod = obj => {
    let cnt = 0;
    
    for(let i = obj.min; i <= obj.max; i++){
        let numberArray = String(i).split('').map(Number);
        let evenSum = numberArray.filter((el) => el%2 === 0).reduce((sum,el) => {return sum + el},0);
        let oddSum = numberArray.filter((el) => el%2 !== 0).reduce((sum,el) => {return sum + el},0); 
        if(evenSum == oddSum){
            cnt++;
        }
    }
    return cnt;
}

let test = getHappyTicket({min:100000,max:200000});
console.log(test);