const palindrom = string => {
    let error
    error = validateNumber(string)
    if(error){	return error}
    let palindrom = string.toString().split('')
    let result = palindromOdd(palindrom).concat(palindromEven(palindrom))
    if(result.length === 0){
        return 0
    }
    else{
        return result
    }
}

const palindromOdd = str => {
    let palindrom = new Array()
    for(let i = 0; i < str.length; i++){
        let leftItem = i - 1, rightItem = i + 1, cnt = 0
        while(leftItem >= 0 && rightItem < str.length && str[leftItem] === str[rightItem]){
            --leftItem
            ++rightItem
            cnt++
        }
        if(cnt !== 0){
            palindrom.push(str.slice(leftItem+1,rightItem))
        }
    }
    return palindrom
}

const palindromEven = str => {
    let palindrom = new Array()
    for(let i = 0; i < str.length; i++){
        let leftItem = i, rightItem = i + 1, cnt = 0
        while(leftItem >= 0 && rightItem < str.length && str[leftItem] === str[rightItem]){
            --leftItem
            ++rightItem
            cnt++
        }
        if(cnt !== 0){
            palindrom.push(str.slice(leftItem+1,rightItem))
        }
    }
    return palindrom
}

const validateNumber = (number) => {
	let error = false
        if(typeof number !== 'number'){
            error = {
                status: 'failed',
                reason: 'Необходимо ввести число'
            }
        }
    return error
}

let pal = palindrom('123443254645121')
console.log(pal)