const triangle = (...triangles) => {
    let error = false
    triangles.forEach(el => {
        el = validateObject(el)
        if(el.status === 'failed') { error = el }
    })
    if(error) { return error}
    triangles.sort(sortTriangle).reverse()
    let trianglesName = triangles.map(function(triangle){
        return triangle.vertices
    })
    return trianglesName
}

const sortTriangle = (first,second) => {
	return first.area - second.area
}

const validateObject = (object) => {
    if(typeof object !== 'object'){
        return {
            status: 'failed',
            reason: 'Нужно передать объект'
        }
    }
    if(typeof object.vertices !== 'string'){
        return {
            status: 'failed',
            reason: 'Название треугольника должно быть строкой'
        }
    }
    if(typeof object.a !== 'number' || typeof object.b !== 'number' || typeof object.c !== 'number'){
        return {
            status: 'failed',
            reason: `Параметры триугольника '${object.vertices}' должны быть number`
        }
    }
    let p = 1/2 * (object.a + object.b + object.c)
    let s = Math.sqrt(p*(p-object.a)*(p-object.b)*(p-object.c))
    if( isNaN(s) || s === 0){
        return {
            status: 'failed',
            reason: `Параметры триугольника '${object.vertices}' указаны неверно`
        } 
    }
    object.area = s
    return object
}

let first = {
    vertices: 'BCD',
    a: 10,
    b: 12,
    c: 9,
}

let second = {
    vertices: 'CDE',
    a: 2,
    b: 3,
    c: 4,
}

let third = {
    vertices: 'ABC',
    a: 12,
    b: 20,
    c: 20.5,
}

let arr = triangle(first,second,third)
console.log(arr)